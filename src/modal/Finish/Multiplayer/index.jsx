import { useDispatch, useSelector } from "react-redux"
import { StyledCol, StyledTableHeader, StyledTableRow } from "./styled"
import { StyledFinish } from "../styled";
import { roomHostSelector, roomIdSelector, roomMembersSelector } from "../../../store/room/selectors"
import { StyledButton, StyledDangerButton, StyledButtonGroup } from "../../../components/styled";
import { socket } from "../../../services/socket";
import { userProfileSelector } from "../../../store/user/selectors";
import { useEffect } from "react";
import { useNavigate } from 'react-router-dom';
import { clearGame, resetGameTimer, setGameTimerStarted, setRoom } from "../../../store/room/slice";
import { resetStatistics } from "../../../store/statistics/slice";
import { clearRoom } from "../../../store/room/slice";

const Finish = () => {
    const roomMembers = useSelector(roomMembersSelector);
    const roomId = useSelector(roomIdSelector);
    const roomHost = useSelector(roomHostSelector);
    const authorized = useSelector(userProfileSelector);

    const dispatch = useDispatch();
    const navigate = useNavigate();

    const handleLeaveRoom = () => {
        socket.disconnect();
        dispatch(clearRoom());
    }

    const handleNewGame = () => {
        socket.emit('restartGame', roomId);
    }

    useEffect(() => {
        const onRestartedGame = (roomInfo) => {
            dispatch(resetStatistics());
            dispatch(clearGame());
            dispatch(setRoom(roomInfo));
            dispatch(resetGameTimer());
            dispatch(setGameTimerStarted(Date.now()));
            navigate('/room/u');
        }

        socket.on('restartedGame', onRestartedGame);

        return () => {
            socket.off('restartedGame', onRestartedGame);

        }
    }, [dispatch, navigate])

    return (
        <StyledFinish>
            <StyledTableHeader>
                <StyledCol>Member</StyledCol>
                <StyledCol>Accuracy</StyledCol>
                <StyledCol>Speed in chars</StyledCol>
                <StyledCol>Speed in words</StyledCol>
            </StyledTableHeader>
            {roomMembers.length &&
                roomMembers.map(({ userId, color, name, speedWPM, speedCPM, accuracy }) => (
                    <StyledTableRow key={userId} color={color} >
                        <StyledCol>{name}</StyledCol>
                        <StyledCol>{accuracy} %</StyledCol>
                        <StyledCol>{speedCPM} cpm</StyledCol>
                        <StyledCol>{speedWPM} wpm</StyledCol>
                    </StyledTableRow>
                ))
            }
            <StyledButtonGroup>
                <StyledDangerButton onClick={handleLeaveRoom}>Leave Room</StyledDangerButton>
                {authorized && authorized.userId === roomHost &&
                    <StyledButton onClick={handleNewGame}>New game</StyledButton>
                }
            </StyledButtonGroup>
        </StyledFinish>
    )
}

export default Finish