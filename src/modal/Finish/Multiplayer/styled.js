import styled from "styled-components";
import { colors, darkColors } from "../../../assets/colors";
import { buttonFont } from "../../../assets/fonts";
import { borderRadius } from "../../../assets/borderRadius";

const TableRow = styled.div`
display: flex;
align-self: stretch;
padding: 0.9rem;
justify-content: space-between;
`;

export const StyledTableHeader = styled(TableRow)``;

export const StyledCol = styled.div`
flex: 1;
color: ${colors.black};
${buttonFont}
`;

export const StyledTableRow = styled(TableRow)`
background: ${colors.white};
${borderRadius}

& div {
    color: ${({ color }) => color ? darkColors[color] : colors.black};
}
`;