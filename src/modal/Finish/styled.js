import styled from "styled-components";
import { Board } from "../../components/styled";
import { modalShadow } from "../../assets/shadows";

export const StyledFinish = styled(Board)`
width: 70%;
position: absolute;
top: 50%;
left: 50%;
transform: translate(-50%, -50%);
${modalShadow}
z-index: 10;
flex-direction: column;
padding: 1rem;
gap: 0.5rem;
`;