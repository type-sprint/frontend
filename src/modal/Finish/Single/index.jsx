import { useDispatch, useSelector } from 'react-redux';
import {
    setTimerExpired,
} from '../../../store/timer/slice';
import { StyledFinish } from '../styled';
import { StyledWPM, StyledResultBlock, StyledAccuracy, StyledLevelSymbol } from './styled';
import { StyledButton } from '../../../components/styled';
import { statisticsSelector } from '../../../store/statistics/selectors';
import { levelToSymbol, levelToWord } from '../../../utils/levels';

const Finish = () => {
    const dispatch = useDispatch();

    const { speedWPM, speedCPM, accuracy, level } = useSelector(statisticsSelector);

    const handleRestartTimer = () => {
        dispatch(setTimerExpired(false));
    }

    return (
        <StyledFinish>
            <StyledLevelSymbol>
                {levelToSymbol[level]}
                {levelToWord[level]}
            </StyledLevelSymbol>
            <StyledResultBlock>
                Wow! You type with the speed of <StyledWPM>{speedWPM}WPM</StyledWPM> ({speedCPM}CPM). Your accuracy was <StyledAccuracy>{accuracy}%</StyledAccuracy>. Keep training!
            </StyledResultBlock>
            <StyledButton onClick={handleRestartTimer}>Restart</StyledButton>

        </StyledFinish>
    )

}

export default Finish