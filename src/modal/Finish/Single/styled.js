import styled from "styled-components";
import { colors } from "../../../assets/colors";
import { buttonFont, propertyFont, subSloganFont } from "../../../assets/fonts";

export const StyledWPM = styled.span`
background: ${colors.secondary}99;
border-radius: 0.2rem;
${buttonFont}
padding: 0.1rem;
`;

export const StyledResultBlock = styled.div`
${subSloganFont}
color: ${colors.black};
`;

export const StyledAccuracy = styled.span`
${buttonFont};
`;

export const StyledLevelSymbol = styled.div`
${propertyFont}
color: ${colors.black};
`;