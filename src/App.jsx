import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Home from './pages/Home'
import Header from './pages/Header';
import Callback from './pages/Callback';
import './App.css'
import CreateRoom from './pages/CreateRoom';
import Room from './pages/Room';
import NotificationsBlock from './components/NotificationsBlock';
import Login from './pages/Login';
import Game from './pages/Game';

const App = () => {


    return (
        <Router>
            <Header />
            <Routes>
                <Route path='/' element={<Home />} />
                <Route path='/login' element={<Login />} />
                <Route path='/callback' element={<Callback />} />
                <Route path='/room' element={<CreateRoom />} />
                <Route path='/room/u' element={<Room />} />
                <Route path='/room/:roomId' element={<Room />} />
                <Route path='/game' element={<Game />} />
            </Routes>

            <NotificationsBlock />
        </Router>
    );
};

export default App;
