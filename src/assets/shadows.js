export const basicShadow = `
box-shadow: 0px 0px 100px rgba(0, 0, 0, 0.2);
`;

export const modalShadow = `
box-shadow: 0px 0px 0px 1000px rgba(0, 0, 0, 0.35);
`;