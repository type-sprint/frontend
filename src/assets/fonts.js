export const buttonFont = `
font-style: normal;
font-weight: 800;
font-size: 18px;
line-height: 22px;
`;

export const logoFont = `
font-style: normal;
font-weight: 800;
font-size: 32px;
line-height: 40px;
`;

export const sloganFont = `
font-style: normal;
font-weight: 700;
font-size: 48px;
line-height: 60px;
`;

export const subSloganFont = `
font-style: normal;
font-weight: 400;
font-size: 20px;
line-height: 25px;
`;

export const typingFont = `
font-family: 'Arvo';
font-style: normal;
font-weight: 400;
font-size: 48px;
line-height: 60px;
`;

export const timeFont = `
font-style: normal;
font-weight: 700;
font-size: 64px;
line-height: 80px;
`;

export const plainFont = `
font-style: normal;
font-weight: 700;
font-size: 16px;
line-height: 20px;
`;

export const propertyFont = `
font-style: normal;
font-weight: 700;
font-size: 36px;
line-height: 45px;
`;

export const h2Font = `
ont-style: normal;
font-weight: 700;
font-size: 20px;
line-height: 25px;
`;

export const notificationFont = `
font-style: normal;
font-weight: 300;
font-size: 16px;
line-height: 20px;
`;
