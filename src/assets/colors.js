export const colors = {
    primary: '#81559B',
    secondary: '#9D79BC',
    danger: '#F26157',
    black: '#1F1D1D',
    white: '#F7F7F9',
    success: 'rgba(173, 242, 175, 0.6)',
    fail: 'rgba(253, 135, 135, 0.6)',
    info: 'rgba(255, 218, 122, 0.6)',
};

export const darkColors = {
    orange: 'rgba(175, 112, 53, 1)',
    pink: 'rgba(137, 106, 124, 1)',
    red: 'rgba(159, 74, 93, 1)',
    skyBlue: 'rgba(70, 131, 164, 1)',
    yellow: 'rgba(175, 165, 59, 1)',
    green: 'rgba(85, 124, 67, 1)',
    blue: 'rgba(52, 77, 166, 1)',
}

export const lightColors = {
    yellow: 'rgba(255, 245, 139, 1)',
    skyBlue: 'rgba(150, 211, 244, 1)',
    blue: 'rgba(132, 157, 246, 1)',
    green: 'rgba(165, 204, 147, 1)',
    orange: 'rgba(255, 192, 133, 1)',
    pink: 'rgba(217, 186, 204, 1)',
    red: 'rgba(239, 154, 173, 1)',
}