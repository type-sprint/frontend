export const sliceSelector = slice => state => state[slice.name] ?? slice.getInitialState();
