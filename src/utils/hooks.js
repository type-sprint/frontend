import { useState } from 'react';
import { useLocation } from 'react-router-dom';

export const useToggle = (initialValue = false) => {
    const [value, setValue] = useState(initialValue);

    const toggle = () => {
        setValue((prevValue) => !prevValue);
    };

    return [value, toggle];
};

export const useSingleGame = () => {
    const { pathname } = useLocation();
    return pathname === '/'
}
