export const shuffle = (array) => {
    const shuffled = [...array];
    shuffled.forEach((_, i) => {
        let j = Math.floor(Math.random() * (i + 1));
        [shuffled[i], shuffled[j]] = [shuffled[j], shuffled[i]];
    })
    return shuffled
}