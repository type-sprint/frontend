const cos = Math.cos;
const sin = Math.sin;
const pi = Math.PI;

const matrixTimes = (([[a, b], [c, d]], [x, y]) => [a * x + b * y, c * x + d * y]);
const rotateMatrix = (x => [[cos(x), -sin(x)], [sin(x), cos(x)]]);
const vecAdd = (([a1, a2], [b1, b2]) => [a1 + b1, a2 + b2]);
const degToRad = d => d * (pi / 180)

const svgEllipseArc = ((timerHeight, timerWidth, angle, strokeWidth) => {
    const cx = timerWidth / 2,
        cy = timerHeight / 2,
        rx = (timerWidth - strokeWidth) / 2,
        ry = (timerHeight - strokeWidth) / 2,
        fi = 0;

    /* [
    returns a SVG path element that represent a ellipse.
    cx,cy → center of ellipse
    rx,ry → major minor radius
    t1 → start angle, in radian.
    angle → angle to sweep, in radian. positive.
    fi → rotation on the whole, in radian
    URL: SVG Circle Arc http://xahlee.info/js/svg_circle_arc.html
    Version 2019-06-19
     ] */

    const t1 = degToRad(-90);
    angle = degToRad(angle);
    const rotMatrix = rotateMatrix(fi);
    const [sX, sY] = (vecAdd(matrixTimes(rotMatrix, [rx * cos(t1), ry * sin(t1)]), [cx, cy]));
    const [eX, eY] = (vecAdd(matrixTimes(rotMatrix, [rx * cos(t1 + angle), ry * sin(t1 + angle)]), [cx, cy]));
    const fA = ((angle > pi) ? 1 : 0);
    const fS = ((angle > 0) ? 1 : 0);
    return "M " + sX + " " + sY + " A " + [rx, ry, fi / (2 * pi) * 360, fA, fS, eX, eY].join(" ");
});

export default svgEllipseArc