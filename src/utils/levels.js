import { symbols } from "../assets/symbols";

const { chocolateBar, thirdPlaceMedal, secondPlaceMedal, firstPlaceMedal } = symbols;

export const levelToSymbol = {
    0: firstPlaceMedal,
    1: secondPlaceMedal,
    2: thirdPlaceMedal,
    3: chocolateBar
}

export const levelToWord = {
    0: 'Expert',
    1: 'Intermediate',
    2: 'Beginner',
    3: 'Baby'
}