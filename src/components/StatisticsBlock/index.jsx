import { useSelector } from 'react-redux';
import TimerSingle from '../Timer/Single';
import TimerMultiplayer from '../Timer/Multiplayer';
import { StyledWrapper, Property, PropertyBlock } from './styled';
import { userProfileSelector } from '../../store/user/selectors';
import { statisticsSelector } from '../../store/statistics/selectors';
import { useSingleGame } from '../../utils/hooks';
import { levelToSymbol, levelToWord } from '../../utils/levels';

const StatisticsBlock = () => {
    const { speedWPM, accuracy, speedCPM, level } = useSelector(statisticsSelector);
    const authorized = useSelector(userProfileSelector);
    const isSingle = useSingleGame();

    return (
        <StyledWrapper>
            {authorized &&
                <PropertyBlock>
                    <Property>
                        {speedCPM}
                    </Property>
                    cpm
                </PropertyBlock>
            }
            <PropertyBlock>
                <Property>
                    {speedWPM}
                </Property>
                wpm
            </PropertyBlock>

            {isSingle ? <TimerSingle /> : <TimerMultiplayer />}

            <PropertyBlock>
                <Property>
                    {accuracy}
                </Property>
                %
            </PropertyBlock>

            {authorized &&
                <PropertyBlock>
                    <Property>
                        {levelToSymbol[level]}
                    </Property>
                    {levelToWord[level]}
                </PropertyBlock>
            }

        </StyledWrapper>
    )
}

export default StatisticsBlock