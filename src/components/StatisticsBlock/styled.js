import styled from "styled-components";
import { plainFont, propertyFont } from "../../assets/fonts";
import { colors } from "../../assets/colors";

export const StyledWrapper = styled.div`
display: flex;
flex-direction: row;
justify-content: center;
align-items: center;
padding: 0px;
width: 100%;
z-index: 10;
gap: 1rem;
`;

export const PropertyBlock = styled.div`
display: flex;
flex-direction: column;
align-items: center;
justify-content: center;
padding: 0px;

width: 8rem;
height: 8rem;

background: #FFFFFF;
border-radius: 50%;

${plainFont};
color: ${colors.black};
z-index: 10;
position: relative;
`;

export const Property = styled.div`
    color: ${colors.black};
    ${propertyFont}
`;