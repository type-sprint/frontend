import styled from "styled-components";
import { Board, Ul } from "../styled";

export const StyledGameDashboard = styled(Board)`
padding: 0.5rem 2rem;
width: 80%;
margin: 1rem 0.5rem 0;
justify-content: stretch;
`;

export const StyledPlayerList = styled(Ul)`
gap: 0.5rem;
display: flex;
flex-direction: column;
align-self: stretch;
width: 100%;
`;