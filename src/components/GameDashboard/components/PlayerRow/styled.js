import styled from "styled-components";
import { colors, darkColors, lightColors } from "../../../../assets/colors";
import { buttonFont } from "../../../../assets/fonts";
import { borderRadius } from "../../../../assets/borderRadius";

export const StyledRow = styled.li`
display: flex;
padding: 0.9rem;
justify-content: flex-end;
align-items: center;
gap: 0.625rem;
width: 100%;
background: ${colors.white};
${borderRadius}
`;

export const StyledText = styled.span`
color: ${({color}) => darkColors[color]};
text-align: right;
${buttonFont}
`;


export const StyledName = styled(StyledText)`
width: 7rem;
text-align: right;
overflow: hidden;
text-overflow: ellipsis;
`;


export const StyledProgressSlider = styled.div`
width: 100%;
flex: 1 0 0;
position: relative;

align-self: stretch;

${borderRadius}
background: #FFF;
`;

export const StyledProgressFill = styled.div`
height: 100%;
background-color: ${({color}) => lightColors[color]};
border-radius: inherit;
transition: width 0.3s ease;
`;