import { StyledRow, StyledText, StyledName, StyledProgressSlider, StyledProgressFill } from "./styled";
import PropTypes from 'prop-types';

const PlayerRow = ({ player: { name, color, progress, speedWPM } }) => {

    return (<StyledRow>
        <StyledName color={color}>{name}</StyledName>
        <StyledProgressSlider>
            <StyledProgressFill color={color} style={{ width: `${progress}%` }} />
        </StyledProgressSlider>
        <StyledText color={color}>{speedWPM ? speedWPM  : 0} wpm</StyledText>
    </StyledRow>)
}

PlayerRow.propTypes = {
    player: PropTypes.object.isRequired,
};

export default PlayerRow