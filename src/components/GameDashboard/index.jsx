import { useSelector } from "react-redux";
import PlayerRow from "./components/PlayerRow";
import { StyledGameDashboard, StyledPlayerList } from "./styled";
import { roomMembersSelector } from "../../store/room/selectors";

const GameDashboard = () => {
    const players = useSelector(roomMembersSelector);

    return (
        <StyledGameDashboard>
            <StyledPlayerList>
                {players.length &&
                    players.map((p) => (
                <PlayerRow key={p.userId} player={p} />
                ))
                }
            </StyledPlayerList>
        </StyledGameDashboard>
    )
}

export default GameDashboard