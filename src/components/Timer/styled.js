import styled from 'styled-components';
import { colors } from '../../assets/colors';
import { timeFont, plainFont } from '../../assets/fonts';

export const StyledTimer = styled.div`
display: flex;
flex-direction: column;
align-items: center;
justify-content: center;
padding: 0px;

width: 10rem;
height: 10rem;

background: #FFFFFF;
border-radius: 50%;

${plainFont};
color: ${colors.black};
z-index: 10;
position: relative;
`;

export const StyledTime = styled.div`
    color: ${colors.black};
    ${timeFont}
`;

export const StyledSVG = styled.svg`
position: absolute;
stroke: ${colors.primary};
fill: none;
stroke-width: ${(props) => props.strokeWidth};
height: ${(props) => props.timerHeight}px;
width: ${(props) => props.timerWidth}px;
`;
