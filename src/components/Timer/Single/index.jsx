import { useEffect, useState, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
    setTimeLeft,
    clearTimeLeft,
    setTimerExpired,
    setTimerStarted,
} from '../../../store/timer/slice';
import { StyledTimer, StyledTime, StyledSVG } from '../styled';
import svgEllipseArc from '../../../utils/genSvg'
import { timerSelector } from '../../../store/timer/selectors';

const Timer = () => {

    const dispatch = useDispatch();
    const { timeLeft, timerStarted, timerDuration } = useSelector(timerSelector);
    const [timerHeight, setTimerHeight] = useState(null);
    const [timerWidth, setTimerWidth] = useState(null);
    const strokeWidth = 7;

    const timerRef = useRef(null);

    useEffect(() => {
        const timerElement = timerRef.current;
        const computedStyles = window.getComputedStyle(timerElement);
        setTimerHeight(+computedStyles.getPropertyValue('height').slice(0, -2));
        setTimerWidth(+computedStyles.getPropertyValue('width').slice(0, -2));
    }, []);

    useEffect(() => {
        let timerInterval;

        if (timerStarted) {
            const initialTimeLeft = Math.max(timerDuration - (Date.now() - timerStarted), 0);

            dispatch(setTimeLeft(initialTimeLeft));

            timerInterval = setInterval(() => {
                const remainingTime = Math.max(timerDuration - (Date.now() - timerStarted), 0);

                dispatch(setTimeLeft(remainingTime));

                if (remainingTime === 0) {
                    dispatch(setTimerExpired(true));
                    dispatch(clearTimeLeft());
                    dispatch(setTimerStarted(false));
                    clearInterval(timerInterval);
                }
            }, timerDuration / 1000);
        }

        return () => {
            clearInterval(timerInterval);
        };
    }, [dispatch, timerStarted, timerDuration]);

    return (
        <StyledTimer ref={timerRef}>
            <StyledTime>{Math.ceil((timeLeft === null ? timerDuration : timeLeft) / 1000)}</StyledTime>
            seconds
            {timerHeight && timerWidth &&
                <StyledSVG strokeWidth={strokeWidth} timerWidth={timerWidth} timerHeight={timerHeight} >
                    <path
                        d={svgEllipseArc(timerHeight, timerWidth,
                            timeLeft ? timeLeft * 360 / timerDuration : 359.99999,
                            strokeWidth)}
                        strokeLinecap="round"
                        strokeLinejoin="round"></path>
                </StyledSVG>
            }
        </StyledTimer>
    )
}

export default Timer;