import styled from 'styled-components';
import { colors } from '../../assets/colors';
import { typingFont } from '../../assets/fonts';
import { Board } from '../styled';

export const StyledTypingBlock = styled(Board)`
margin: 1rem;
overflow: hidden;
position: relative;
`;

export const Words = styled.div`
display: flex;
width: 50%;
justify-content: left;
gap: 1ch;
`;

export const Word = styled.span`
${typingFont}
color: ${colors.black};
white-space: nowrap;
`;

export const TypedWords = styled(Words)`
justify-content: right;
`;

export const TypedWord = styled(Word)`
${typingFont}
color: ${colors.black}99;
white-space: nowrap;
`;

export const TestInput = styled.div`
caret-color: ${colors.black};
padding-left: 0.25em;
display: inline-block;
text-align: right;
width: fit-content;
${typingFont}
white-space: nowrap;
color: ${colors.primary};
outline: none;
border: none;
`;