import axios from 'axios';
// without credentials
import React, { useState, useEffect, useRef, useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { StyledTypingBlock, TypedWords, TypedWord, Words, Word, TestInput } from '../styled';

import {
    setTimerStarted,
    resetTimer,
} from '../../../store/timer/slice';

import {
    setSpeedWPM,
    resetStatistics,
    setAccuracy,
    setSpeedCPM,
    setLevel
} from '../../../store/statistics/slice'
import { timerSelector } from '../../../store/timer/selectors';
import { moveCaret } from '../../../utils/moveCaret';

const TypingBlock = React.memo(() => {
    const [words, setWords] = useState([]);
    const [typedWords, setTypedWords] = useState([]);
    const [typedPart, setTypedPart] = useState('');
    const [currentWord, setCurrentWord] = useState('');
    const [isCompleted, setCompleted] = useState(false);
    const [isCorrect, setCorrect] = useState(true);
    const [correctChars, setCorrectChars] = useState(0);
    const [totalChars, setTotalChars] = useState(0);

    const inputRef = useRef(null);
    const dispatch = useDispatch();
    const { timerStarted, timerExpired, timeLeft, timerDuration } = useSelector(timerSelector);

    const fetchWords = useCallback(async () => {
        try {
            const { data } = await axios.get('https://random-word-api.vercel.app/api', {
                params: {
                    words: timerDuration / 1000 * 5
                }
            });

            const set = [...new Set(data)];
            setWords(set);
            setCurrentWord(set[0]);
            setTypedWords([]);
            setTypedPart('');
            setCompleted(false);
            setCorrect(false);
            dispatch(resetStatistics());
            dispatch(resetTimer());
            if (inputRef.current) {
                inputRef.current.innerText = '';
            }
            inputRef.current.focus();

        } catch (error) {
            console.error(error);
            throw new Error('Fetch words failed', error.message);
        }
    }, [timerDuration, dispatch]);

    // when restart timer reset words set
    useEffect(() => {
        if (!timerExpired && !timerStarted && timeLeft === null) {
            fetchWords();
        }
    }, [timerStarted, timerExpired, timeLeft, fetchWords]);

    useEffect(() => {
        dispatch(setAccuracy({ correctChars, totalChars }));
        dispatch(setLevel());
    }, [correctChars, totalChars, dispatch]);

    const handleTypingWord = (e) => {
        const { target, nativeEvent } = e;

        const letter = target.innerText.slice(-1);
        // if there are no more words
        if (!words.length) return

        const [currentPart, ...rest] = words;

        if (letter === currentPart[0]) {
            // correct typing
            if (typedPart + currentPart === currentWord) {
                // word is correct
                setCorrect(true);
                setWords([... new Set([currentPart.slice(1), ...rest])]);
                setCorrectChars(s => s + 1);
                dispatch(setSpeedCPM({ chars: correctChars + 1, timeLeft, timerDuration }));
                if (currentPart.length === 1) {
                    // if the end of the word
                    setCompleted(true);
                }
            }
        } else if (nativeEvent.inputType === 'deleteContentBackward') {
            // if deleted 
            const deleted = typedPart.slice(target.innerText.length);

            if (target.innerText + deleted + currentPart === currentWord) {
                // if deleted correct char
                setWords([(deleted + currentPart), ...rest]);

            } else if (target.innerText + currentPart === currentWord) {
                // if deleted and became correct
                setCorrect(true);
            }
            setCorrectChars(s => s + 1);
            dispatch(setSpeedCPM({ chars: correctChars + 1, timeLeft, timerDuration }));
        } else if (letter.trim() === '') {
            if (isCompleted) {
                // the word is completed (last one is space char)
                dispatch(setSpeedWPM({ words: typedWords.length + 1, timeLeft, timerDuration }));
                setCompleted(false);
                target.innerText = '';
                setTypedWords(prev => [...prev, currentWord]);
                setCurrentWord(rest[0]);
                setWords(rest);
                setCorrectChars(s => s + 1);
            } else {
                // just space or some space char 
                target.innerText = target.innerText.trim();
            }
        } else {
            // if incorrect typing
            setCorrect(false);
        }
        setTotalChars(s => s + 1);

        // move caret to the end of input
        moveCaret(inputRef.current);

        setTypedPart(target.innerText);

        if (!timerStarted && !timerExpired) {
            dispatch(setTimerStarted(Date.now()));
        }
    }

    return (
        <StyledTypingBlock>

            <TypedWords>
                {typedWords &&
                    typedWords.map(w => (
                        <TypedWord key={w}>{w}</TypedWord>
                    ))}
                <TestInput
                    tabIndex="1"
                    contentEditable={!timerExpired}
                    autoComplete="off"
                    autoCorrect="off"
                    autoCapitalize="off"
                    spellCheck="false"
                    onInput={handleTypingWord}
                    ref={inputRef}
                    suppressContentEditableWarning={true}
                    style={{ textDecoration: isCorrect ? 'none' : 'line-through' }}
                >
                </TestInput>
            </TypedWords>

            <Words>
                {words &&
                    words.map(w => (
                        <Word key={w}>{w}</Word>
                    ))}
            </Words>
        </StyledTypingBlock>
    )
})
TypingBlock.displayName = 'TypingBlock'

export default TypingBlock;