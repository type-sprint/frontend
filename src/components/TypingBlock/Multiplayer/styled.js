import styled from "styled-components";
import { plainFont } from "../../../assets/fonts";
import { colors } from "../../../assets/colors";

export const StyledGameInfo = styled.div`
position: absolute;
top: 0.5rem;
${plainFont}
color: ${colors.primary};
`;