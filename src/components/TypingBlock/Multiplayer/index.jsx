import { useEffect, useRef, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { StyledTypingBlock, TypedWords, TypedWord, Words, Word, TestInput } from '../styled';
import { StyledGameInfo } from './styled';
import { socket } from '../../../services/socket';

import {
    setSpeedWPM,
    setAccuracy,
    setSpeedCPM
} from '../../../store/statistics/slice'
import { roomGameSelector, roomIdSelector, roomMembersSelector, roomTimerSelector } from '../../../store/room/selectors';
import {
    setGameWords,
    setGameTypedWords,
    setGameTypedPart,
    setGameCurrentWord,
    setGameIsCompleted,
    setGameIsCorrect,
    setGameCorrectChars,
    setGameTotalChars,
    setGameProgress,
    setGameMembersProgress,
} from '../../../store/room/slice';
import { moveCaret } from '../../../utils/moveCaret';
import { statisticsSelector } from '../../../store/statistics/selectors';
import { userIdSelector } from '../../../store/user/selectors';

const TypingBlock = () => {
    const { words,
        typedWords,
        typedPart,
        currentWord,
        isCompleted,
        isCorrect,
        correctChars,
        totalChars,
        progress } = useSelector(roomGameSelector);

    const statisticsData = useSelector(statisticsSelector);
    const roomId = useSelector(roomIdSelector);

    const inputRef = useRef(null);
    const dispatch = useDispatch();
    const { timerExpired, timeLeft, timerDuration } = useSelector(roomTimerSelector);

    const members = useSelector(roomMembersSelector);
    const userId = useSelector(userIdSelector);

    const [place, setPlace] = useState('');
    const [currentLetter, setCurrentLetter] = useState(null);
    const [correctLetter, setCorrectLetter] = useState(null);
    const [action, setAction] = useState(null);

    useEffect(() => {
        moveCaret(inputRef.current);
        inputRef.current.focus();
    }, [dispatch])

    useEffect(() => {
        socket.emit('updateProgressAndSpeed', roomId, progress, { ...statisticsData, currentLetter, correctLetter, action });
    }, [dispatch, typedPart])

    useEffect(() => {
        const onUpdatedProgressAndSpeed = (data) => {
            dispatch(setGameMembersProgress(data));
        }
        socket.on('updatedProgressAndSpeed', onUpdatedProgressAndSpeed);

        return () => {
            socket.off('updatedProgressAndSpeed', onUpdatedProgressAndSpeed);
        }
    }, [dispatch])

    useEffect(() => {
        const place = {
            1: 'st',
            2: 'nd',
            3: 'rd',
        }
        const p = members.findIndex(m => m.userId === userId) + 1;
        const lastChar = p.toString().slice(-1);
        setPlace(p + (place[lastChar] || 'th'));
    }, [progress, members, userId])

    const handleTypingWord = (e) => {
        const { target, nativeEvent } = e;

        const letter = target.innerText.slice(-1);
        // if there are no more words
        if (!words.length) return

        dispatch(setGameTotalChars(totalChars + 1));
        const [currentPart, ...rest] = words;
        setCorrectLetter(currentPart[0]);

        if (letter === currentPart[0]) {
            // correct typing
            if (typedPart + currentPart === currentWord) {
                // word is correct
                dispatch(setGameIsCorrect(true));
                dispatch(setGameWords([... new Set([currentPart.slice(1), ...rest])]));
                dispatch(setGameCorrectChars(correctChars + 1));
                dispatch(setSpeedCPM({ chars: correctChars, timeLeft, timerDuration }));
                setCurrentLetter(letter);
                setAction('correct');
                if (currentPart.length === 1) {
                    // if the end of the word
                    dispatch(setGameIsCompleted(true));
                }
            }
        } else if (nativeEvent.inputType === 'deleteContentBackward') {
            // if deleted 
            target.innerText = target.innerText.trim();
            const deleted = typedPart.slice(target.innerText.length);
            setAction('delete');
            setCurrentLetter(null);

            if (target.innerText + deleted + currentPart === currentWord) {
                // if deleted correct char
                dispatch(setGameWords([(deleted + currentPart), ...rest]));
                setAction('deletedCorrect');

            } else if (target.innerText + currentPart === currentWord) {
                // if deleted and became correct
                dispatch(setGameIsCorrect(true));
            }
            dispatch(setGameCorrectChars(correctChars + 1));
            dispatch(setSpeedCPM({ chars: correctChars, timeLeft, timerDuration }));
        } else if (letter.trim() === '') {
            if (isCompleted) {
                // the word is completed (last one is space char)
                dispatch(setSpeedWPM({ words: typedWords.length, timeLeft, timerDuration }));
                dispatch(setGameIsCompleted(false));
                target.innerText = '';
                dispatch(setGameTypedWords([...typedWords, currentWord]));
                dispatch(setGameCurrentWord(rest[0]));
                dispatch(setGameWords(rest));
                dispatch(setGameCorrectChars(correctChars + 1));
                setAction('completeWord')
                setCurrentLetter(null);

            } else {
                // just space or some space char 
                target.innerText = target.innerText.trim();
            }
        } else {
            // if incorrect typing
            dispatch(setGameIsCorrect(false));
            setAction('incorrect');
        }

        // move caret to the end of input
        moveCaret(inputRef.current);
        dispatch(setGameTypedPart(target.innerText));

        dispatch(setAccuracy({ correctChars, totalChars }));
        dispatch(setGameProgress());
    }

    return (
        <StyledTypingBlock>
            <StyledGameInfo>
                {place} place
            </StyledGameInfo>
            <TypedWords>
                {typedWords &&
                    typedWords.map(w => (
                        <TypedWord key={w}>{w}</TypedWord>
                    ))}
                <TestInput
                    tabIndex="1"
                    contentEditable={!timerExpired}
                    autoComplete="off"
                    autoCorrect="off"
                    autoCapitalize="off"
                    spellCheck="false"
                    onInput={handleTypingWord}
                    ref={inputRef}
                    suppressContentEditableWarning={true}
                    style={{ textDecoration: isCorrect ? 'none' : 'line-through' }}
                >
                    {typedPart}</TestInput>
            </TypedWords>

            <Words>
                {words &&
                    words.map(w => (
                        <Word key={w}>{w}</Word>
                    ))}
            </Words>
        </StyledTypingBlock>
    )
}

TypingBlock.displayName = 'TypingBlock'

export default TypingBlock;