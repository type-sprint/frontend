import styled from "styled-components";
import { subSloganFont, sloganFont, h2Font, buttonFont } from "../assets/fonts";
import { colors } from "../assets/colors";
import { basicShadow } from "../assets/shadows";
import { borderRadius } from "../assets/borderRadius";

export const Page = styled.div`
padding: auto 0;
display: flex;
flex-direction: column;
align-items: center;
`;

export const Board = styled.div`
display: flex;
flex-direction: row;
justify-content: center;
align-items: center;
padding: 2rem 0;
${basicShadow}
${borderRadius}
max-width: 1200px;
width: 100%;
background: #FFFFFF;
`;

export const H3 = styled.h3`
${subSloganFont}
color: ${colors.black};
`;

export const H1 = styled.h1`
${sloganFont}
color: ${colors.primary};
`;

export const H2 = styled.h2`
${h2Font}
color: ${colors.black};
`;

export const ErrorH1 = styled(H1)`
color: ${colors.black};
font-size: x-large;

`;

export const Ul = styled.ul`
padding: 0;
display: flex;
justify-content: center;
align-items: center;
`;

export const StyledButton = styled.button`
display: flex;
flex-direction: row;
justify-content: center;
align-items: center;
padding: 0.5rem 1rem;

${buttonFont}
background: ${colors.secondary};
border: 2px solid ${colors.secondary};
color: ${colors.white};

border-radius: 10px;
`;

export const StyledDangerButton = styled(StyledButton)`
background: transparent;
color: ${colors.danger};
border: 2px solid ${colors.danger};
`;

export const StyledButtonGroup = styled.div`
display: flex;
justify-content: space-between;
align-items: center;
align-self: stretch;
margin-top:2rem;
`;