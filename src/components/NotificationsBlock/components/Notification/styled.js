import styled, { keyframes } from 'styled-components';
import { colors } from '../../../../assets/colors';
import { borderRadius } from '../../../../assets/borderRadius';
import DoneRoundedIcon from '@mui/icons-material/DoneRounded';
import TipsAndUpdatesOutlinedIcon from '@mui/icons-material/TipsAndUpdatesOutlined';
import SentimentDissatisfiedRoundedIcon from '@mui/icons-material/SentimentDissatisfiedRounded';
import { notificationFont } from '../../../../assets/fonts';

const typeToColor = (type) => colors[type] || colors.white;

const slideUpAnimation = keyframes`
  0% {
    transform: translateX(-100%);
    opacity: 0;
  }
  100% {
    transform: translateX(0);
    opacity: 1;
  }
`;

const slideOutAnimation = keyframes`
  0% {
    transform: translatex(0);
    opacity: 1;
  }
  100% {
    transform: translateX(-100%);
    opacity: 0;
  }
`;


export const StyledNotification = styled.div`
display: flex;
flex-direction: row;
align-items: center;
padding: 1rem 1.5rem;
gap: 1rem;
width: 15rem;

background: ${({ type }) => typeToColor(type)};
color: ${colors.black};
${borderRadius}
${notificationFont}

animation-name: ${slideUpAnimation};
animation-duration: 0.2s;
animation-timing-function: ease-in-out;
animation-fill-mode: forwards;

&.slide-out {
    animation-name: ${slideOutAnimation};
    animation-duration: 0.5s;
    animation-timing-function: ease-out;
    animation-fill-mode: forwards;
  }
`;

export const StyledIconWrapper = styled.div`
display: flex;
flex-direction: row;
justify-content: center;
align-items: center;
padding: 0.5rem;

background: ${({ type }) => typeToColor(type)};
${borderRadius}
`;

export const StyledSuccessIcon = styled(DoneRoundedIcon)`
color: ${colors.black};
`;

export const StyledFailIcon = styled(SentimentDissatisfiedRoundedIcon)`
color: ${colors.black};
`;

export const StyledInfoIcon = styled(TipsAndUpdatesOutlinedIcon)`
color: ${colors.black};
`;