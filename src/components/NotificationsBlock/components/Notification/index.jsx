import { useEffect, useState } from "react";
import { StyledNotification, StyledIconWrapper, StyledSuccessIcon, StyledFailIcon, StyledInfoIcon } from "./styled";
import PropTypes from 'prop-types';

const iconByType = {
    success: <StyledSuccessIcon />,
    fail: <StyledFailIcon />,
    info: <StyledInfoIcon />,
}

const Notification = ({ type, message }) => {
    const [hide, setHide] = useState(false);

    const IconComponent = iconByType[type];

    useEffect(() => {
        setTimeout(() => {
            setHide(true);
            setTimeout(() => {
                setHide(false);
            }, 500);
        }, 4500)
    })

    return (
        <StyledNotification type={type} className={hide ? 'slide-out' : ''}>
            <StyledIconWrapper type={type}>
                {IconComponent ?? <IconComponent />}
            </StyledIconWrapper>
            {message}
        </StyledNotification>
    )
}

Notification.propTypes = {
    type: PropTypes.string.isRequired,
    message: PropTypes.string.isRequired,
};

export default Notification