import { StyledNotificationsBlock } from "./styled";

import { notificationListSelector } from '../../store/notifications/selectors';
import { useSelector } from 'react-redux';
import Notification from "./components/Notification";

const NotificationsBlock = () => {
    const notifications = useSelector(notificationListSelector);
    
    return (
        <StyledNotificationsBlock>
            {notifications &&
                Object.entries(notifications).map(([id, { type, message }]) => {
                    return (
                        <Notification key={id} type={type} message={message} />
                    )
                })
            }
        </StyledNotificationsBlock>
    )
}

export default NotificationsBlock