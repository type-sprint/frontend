import styled from "styled-components";

export const StyledNotificationsBlock = styled.aside`
display: flex;
flex-direction: column;
justify-content: center;
align-items: center;
gap: 16px;

position: absolute;
left: 1rem;
bottom: 1rem;
z-index: 10;
`;