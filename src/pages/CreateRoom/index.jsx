import { Page, ErrorH1, H3 } from '../../components/styled'
import { StyledRoomBlock } from './styled'
import { StyledButton } from '../../components/styled';

import { useDispatch, useSelector } from 'react-redux';
import { userProfileSelector } from '../../store/user/selectors';
import { roomCreatedSelector, roomIdSelector } from '../../store/room/selectors';
import { createRoomThunk } from '../../store/room/thunk';
import { useNavigate } from 'react-router-dom';
import { useEffect } from 'react';
import { addNotificationWithTimeout } from '../../store/notifications/thunk';

const CreateRoom = () => {
    const authorized = useSelector(userProfileSelector);
    const dispatch = useDispatch();
    const roomCreated = useSelector(roomCreatedSelector);
    const roomId = useSelector(roomIdSelector);
    const navigate = useNavigate();

    const handleCreateRoom = async () => {
        const created = await dispatch(createRoomThunk());
        if (created.error) {
            navigate('/login');
        }
    }

    useEffect(() => {
        if (roomId && !roomCreated) {
            dispatch(addNotificationWithTimeout({ type: 'info', message: 'You can only have 1 room at a time' }));
            navigate('/room/u');
        }
    }, [dispatch, navigate, roomId, roomCreated])

    useEffect(() => {
        if (roomCreated) {
            navigate('/room/u');
        }
    }, [roomCreated, navigate]);

    useEffect(() => {
        document.title = 'New Room - TypeSprint';
    }, []);

    return (
        <Page>
            {authorized
                ?
                <StyledRoomBlock>
                    <H3>Create your own room, invite friends and on your mark, get set, go!</H3>
                    <StyledButton onClick={handleCreateRoom}>Create</StyledButton>
                    {roomId}
                </StyledRoomBlock>
                :
                <ErrorH1>
                    Sign in to be able to create rooms
                </ErrorH1>
            }
        </Page>
    )
}

export default CreateRoom