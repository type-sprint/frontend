import styled from 'styled-components';
import { Board } from '../../components/styled';

export const StyledRoomBlock = styled(Board)`
padding: 2rem 4rem; 
margin: 5rem;
width: 70%;
justify-content: space-between;
gap: 1rem;
`;