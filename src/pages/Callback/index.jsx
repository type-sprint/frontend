import { useEffect } from 'react';
import axios from '../../services/axios';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { H3 } from '../../components/styled';

import { setUserProfile,clearUserProfile } from '../../store/user/slice';
import { resetTimer } from '../../store/timer/slice';
import { resetStatistics } from '../../store/statistics/slice';
import { clearRoom } from '../../store/room/slice';
import { clearNotifications } from '../../store/notifications/slice';

const Callback = () => {
    const dispatch = useDispatch();
    const navigate = useNavigate();

    useEffect(() => {
        const fetchUserProfile = async () => {
            try {
                const response = await axios.get(import.meta.env.VITE_API_URL + '/profile');
                dispatch(setUserProfile(response.data));
                navigate(-3);
            } catch (error) {
                if (error.response.status === 401) {
                    dispatch(clearUserProfile());
                    dispatch(resetTimer());
                    dispatch(resetStatistics());
                    dispatch(clearRoom());
                    dispatch(clearNotifications());
                    navigate('/');
                }
            }
        };
        fetchUserProfile();

    }, [dispatch, navigate]);

    return (
        <div>
            <H3>Callback Page</H3>
            {/* Render the rest of the page */}
        </div>
    );
};

export default Callback;