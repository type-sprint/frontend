import styled from 'styled-components';
import { colors } from '../../assets/colors';
import { buttonFont, logoFont } from '../../assets/fonts';
import { Link } from 'react-router-dom';
import { basicShadow } from '../../assets/shadows';

export const StyledHeader = styled.header`
display: flex;
flex-direction: row;
justify-content: space-between;
align-items: center;
padding: 1rem 10%;

height: 2rem;
background: #FFFFFF;
`;

export const SignIn = styled.button`
display: flex;
flex-direction: row;
justify-content: center;
align-items: center;
padding: 0.5rem 1rem;

background: ${colors.primary};
border-radius: 10px;
${buttonFont}
`;

export const Logo = styled.div`
${logoFont}
color: ${colors.black};
`;

export const NavUl = styled.ul`
display: flex;
flex-direction: row;
align-items: center;
padding: 0px;
gap: 16px;
`;

export const StyledLink = styled(Link)`
display: flex;
flex-direction: row;
justify-content: center;
align-items: center;
padding: 0.5rem 1rem;

border-radius: 10px;
${buttonFont}
color: ${colors.black};

&:hover {
    background: ${colors.white};
    color: ${colors.primary};
}
`;

export const Profile = styled.div`
display: flex;
flex-direction: row;
align-items: center;
padding: 0px;
gap: 12px;
isolation: isolate;

border-radius: 10px;
height: 100%;
position: relative;
cursor: pointer;
`;

export const StyledName = styled.div`
padding: 0.5rem 1rem;
border-radius: 10px;
${buttonFont}
color: ${colors.primary};
`;

export const StyledImg = styled.img`
height: 150%;
border-radius: 10px;
`;

export const StyledUl = styled.ul`
display: flex;
flex-direction: row;
align-items: flex-start;
padding: 0.5rem;
gap: 10px;

position: absolute;
right: 0;
top: 100%;

background: #FFFFFF;

${basicShadow}
border-radius: 10px;
`;

export const SignOut = styled.button`
padding: 0.5rem 1rem;
${buttonFont}
background: transparent;
border-radius: 10px;
color: ${colors.danger};

&:hover {
    background: ${colors.white};
}
`;