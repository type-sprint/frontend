import { useSelector } from 'react-redux';
import { useState } from 'react';
import { StyledLink, StyledHeader, Logo, NavUl, Profile, SignIn, StyledName, StyledImg, StyledUl, SignOut } from './styled';
import { userProfileSelector } from '../../store/user/selectors';
import { roomStateSelector } from '../../store/room/selectors';
import { Fragment } from 'react';

const Header = () => {
    const handleSignIn = async () => {
        window.location.href = import.meta.env.VITE_API_URL + '/login';
    };

    const handleSignOut = async () => {
        window.location.href = import.meta.env.VITE_API_URL + '/logout'
    }
    const authorized = useSelector(userProfileSelector);
    const { name, photo } = authorized || {};

    const roomState = useSelector(roomStateSelector);
    const [dropdown, toggleDropdown] = useState(false);

    const toggleDropdownHandler = () => {
        toggleDropdown(!dropdown);
    }

    return (
        <StyledHeader>
            <Logo>Type Sprint</Logo>
            <nav>
                <NavUl>
                    <li>
                        <StyledLink to="/">Home</StyledLink>
                    </li>
                    {/* <li>
                                <StyledLink to="/room">Chat Room</StyledLink>
                            </li> */}

                    {authorized &&
                        <Fragment>
                            <li>
                                <StyledLink to="/room">Room</StyledLink>
                            </li>
                            {roomState &&
                                <li>
                                    <StyledLink to="/game">Game</StyledLink>
                                </li>
                            }
                        </Fragment>
                    }
                </NavUl>
            </nav>
            {authorized ?
                <Profile onClick={toggleDropdownHandler}>
                    <StyledName>{name}</StyledName>
                    <StyledImg src={photo} />
                    {dropdown &&
                        <StyledUl>
                            <li>
                                <SignOut onClick={handleSignOut}>Sign Out</SignOut>
                            </li>
                        </StyledUl>
                    }
                </Profile>
                :
                <SignIn onClick={handleSignIn}>Sign In</SignIn>
            }

        </StyledHeader>

    );
};

export default Header;