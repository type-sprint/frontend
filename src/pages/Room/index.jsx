import { Page, H2, StyledButtonGroup, StyledButton, StyledDangerButton } from '../../components/styled'
import { StyledRoomBlock, StyledRowBlock, StyledUrl, StyledCopyIcon, StyledMemberList } from './styled'

import { useDispatch, useSelector } from 'react-redux';
import { userProfileSelector } from '../../store/user/selectors';
import { roomHostSelector, roomHostMemberSelector, roomIdSelector, roomMembersSelector, roomStateSelector } from '../../store/room/selectors';
import { useEffect, useState, Fragment } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { addNotificationWithTimeout } from '../../store/notifications/thunk';
import Member from './components/Member';

import { socket } from '../../services/socket';
import { setRoom, clearRoom } from '../../store/room/slice';
import { setGameTimerStarted } from '../../store/room/slice';

const Room = () => {
    const authorized = useSelector(userProfileSelector);
    const roomId = useSelector(roomIdSelector);
    const paramRoomId = useParams().roomId;
    const roomMembers = useSelector(roomMembersSelector);
    const roomHost = useSelector(roomHostSelector);
    const roomHostMember = useSelector(roomHostMemberSelector);
    const roomState = useSelector(roomStateSelector);
    const navigate = useNavigate();

    const baseUrl = window.location.href.split('/').slice(0, -1).join('/');
    const [url, setUrl] = useState('');

    const dispatch = useDispatch();

    const handleCopyLink = () => {
        navigator.clipboard.writeText(url)
            .then(() => {
                dispatch(addNotificationWithTimeout({ type: 'success', message: 'Link copied to clipboard!' }));
            })
            .catch(() => {
                dispatch(addNotificationWithTimeout({ type: 'fail', message: 'Copying failed:(' }));
            })
    }

    const handleLeaveRoom = () => {
        socket.disconnect();
        dispatch(clearRoom());
    }

    const handleStartGame = () => {
        socket.emit('gameStart', roomId);
    }

    useEffect(() => {
        setUrl([baseUrl, roomId].join('/'));
    }, [roomId, setUrl, baseUrl]);

    useEffect(() => {
        document.title = 'Room - TypeSprint';
        socket.connect();
    }, [])

    useEffect(() => {
        const onConnect = () => {
            socket.emit('joinRoom', roomId || paramRoomId);
        }

        const onRoomMemberUpdate = (roomInfo) => {
            dispatch(setRoom(roomInfo));
        }

        const onAuthError = () => {
            navigate('/login');
        }

        const onInvalidRoom = () => {
            dispatch(addNotificationWithTimeout({ type: 'fail', message: 'This room does not exist' }))
            dispatch(clearRoom());
            navigate('/room');
        }

        const onGameAlreadyStarted = () => {
            dispatch(addNotificationWithTimeout({ type: 'fail', message: 'Game in this room has already been started' }))
            dispatch(clearRoom());
            navigate('/room');
        }

        const onGameStarted = (roomInfo) => {
            dispatch(setRoom(roomInfo));
            dispatch(setGameTimerStarted(Date.now()));
            navigate('/game');
        }

        if (roomId && paramRoomId) {
            navigate('/room/u');
        }

        socket.on('connect', onConnect);
        socket.on('roomMembersUpdate', onRoomMemberUpdate);
        socket.on('authError', onAuthError);
        socket.on('invalidRoom', onInvalidRoom);
        socket.on('gameIsAlreadyStarted', onGameAlreadyStarted);
        socket.on('gameStarted', onGameStarted);

        return () => {
            socket.off('connect', onConnect);
            socket.off('roomMembersUpdate', onRoomMemberUpdate);
            socket.off('authError', onAuthError);
            socket.off('invalidRoom', onInvalidRoom);
            socket.off('gameStarted', onGameStarted);

        }
    }, [dispatch, roomId, paramRoomId, navigate])

    useEffect(() => {
        if (roomState) {
            navigate('/game');
        } else if (!roomId && !paramRoomId) {
            navigate('/room');
        } else if (roomId && paramRoomId) {
            navigate('/room/u');
            dispatch(addNotificationWithTimeout({ type: 'info', message: 'You can only have 1 room at a time' }));
        } else if (!authorized) {
            navigate('/login');
            dispatch(addNotificationWithTimeout({ type: 'info', message: 'Sign in to be able to join rooms' }));
        }
    }, [authorized, dispatch, navigate, paramRoomId, roomId, roomState]);

    return (
        <Page>
            <StyledRoomBlock>
                <H2>Room</H2>
                <StyledRowBlock>
                    <StyledUrl>{url}</StyledUrl>
                    <StyledCopyIcon onClick={handleCopyLink} />
                </StyledRowBlock>

                {roomMembers.length &&
                    <Fragment>
                        <H2>Members</H2>
                        <StyledMemberList>
                            {roomHostMember &&
                                <Member key={roomHost} member={roomHostMember} host={true}></Member>
                            }
                            {roomMembers.map(member => {
                                if (member && member.userId !== roomHost) {
                                    return (<Member key={member.userId} member={member} />)
                                }
                            })}
                        </StyledMemberList>
                    </Fragment>
                }
                <StyledButtonGroup>
                    <StyledDangerButton onClick={handleLeaveRoom}>Leave Room</StyledDangerButton>
                    {authorized && authorized.userId === roomHost &&
                        <StyledButton onClick={handleStartGame}>Start game</StyledButton>
                    }
                </StyledButtonGroup>

            </StyledRoomBlock>
        </Page>
    )
}

export default Room