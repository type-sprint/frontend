import styled from 'styled-components';
import { plainFont } from '../../assets/fonts';
import { colors } from '../../assets/colors';
import { Board } from '../../components/styled';
import { borderRadius } from '../../assets/borderRadius';

import CopyAllIcon from '@mui/icons-material/CopyAll';

export const StyledRoomBlock = styled(Board)`
padding: 2rem 3rem; 
margin: 5rem;
width: 70%;
justify-content: space-between;
align-items: start;
flex-direction: column;
gap: 0.5rem;
`;

export const StyledRowBlock = styled.div`
display: flex;
flex-direction: row;
justify-content: space-between;
align-items: center;
padding: 1rem;
gap: 0.2rem;
align-self: stretch;

background: ${colors.white};
${borderRadius}
`;

export const StyledUrl = styled.span`
${plainFont}
color: ${colors.black};

overflow: hidden;
text-wrap: nowrap;
text-overflow: ellipsis;
white-space: nowrap;
`;

export const StyledCopyIcon = styled(CopyAllIcon)`
color: ${colors.black};
cursor: pointer;

&:hover {
    color: ${colors.primary};
}
`;

export const StyledMemberList = styled.ul`
display: flex;
align-items: flex-start;
align-content: flex-start;
padding: 0;
margin: 0;
gap: 0.75rem;
align-self: stretch;
flex-wrap: wrap;
`;