import styled from "styled-components";
import { borderRadius } from "../../../../assets/borderRadius";
import { colors, darkColors } from "../../../../assets/colors";
import { buttonFont } from "../../../../assets/fonts";
import StarBorderPurple500RoundedIcon from '@mui/icons-material/StarBorderPurple500Rounded';

export const StyledMember = styled.li`
display: flex;
padding: 0.7rem;
justify-content: center;
align-items: center;
gap: 0.625rem;
${borderRadius}
${buttonFont}
color: ${(({ color }) => darkColors[color])};
background: ${colors.white};
`;

export const StyledMemberAvatar = styled.img`
width: 2rem;
height: 2rem;
${borderRadius}
`;

export const StyledHostStar = styled(StarBorderPurple500RoundedIcon)`
    color: ${(({ color }) => darkColors[color])};
`;