import { StyledMember, StyledMemberAvatar, StyledHostStar } from "./styled"
import PropTypes from 'prop-types';

const Member = ({ member: { userId, photo, name, color }, host = false }) => {
    return (
        <StyledMember key={userId} color={color}>
            <StyledMemberAvatar src={photo} />
            {name}
            {host &&
                <StyledHostStar color={color} />
            }
        </StyledMember>
    )
}


Member.propTypes = {
    member: PropTypes.object.isRequired,
    host: PropTypes.bool
};

export default Member