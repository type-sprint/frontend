import styled from "styled-components";
import { StyledDangerButton } from "../../components/styled";
import LogoutRoundedIcon from '@mui/icons-material/LogoutRounded';
import { colors } from "../../assets/colors";
import { borderRadius } from "../../assets/borderRadius";
import { basicShadow } from "../../assets/shadows";

export const StyledLeaveButton = styled(StyledDangerButton)`
align-self: flex-end;
margin-top: 0.5rem;
`;

export const StyledExitButton = styled(LogoutRoundedIcon)`
color: ${colors.danger};
align-self: flex-end;
position absolute;
padding: 0.5rem;
margin: 0.5rem;
cursor: pointer;
background: #fff;
${borderRadius}
${basicShadow}

&:hover {
    background: ${colors.white};
}
`;