import TypingBlock from "../../components/TypingBlock/Multiplayer";
import StatisticsBlock from "../../components/StatisticsBlock";
import { Page } from "../../components/styled";
import { useDispatch, useSelector } from "react-redux";
import GameDashboard from "../../components/GameDashboard";
import { StyledExitButton } from "./styled";
import { socket } from "../../services/socket";
import { clearRoom } from "../../store/room/slice";
import { useEffect } from "react";
import { roomIdSelector, roomTimerSelector } from "../../store/room/selectors";
import { useNavigate } from "react-router-dom";
import Finish from "../../modal/Finish/Multiplayer";

const Game = () => {
    const { timerExpired } = useSelector(roomTimerSelector);
    const roomId = useSelector(roomIdSelector);

    const dispatch = useDispatch();
    const navigate = useNavigate();

    const handleLeaveRoom = () => {
        socket.disconnect();
        dispatch(clearRoom());
    }

    useEffect(() => {
        if (!roomId) {
            navigate('/room');
        }
    }, [roomId, navigate])

    useEffect(() => {
        document.title = 'Game - TypeSprint';
    })
    return (
        <Page>
            <StyledExitButton onClick={handleLeaveRoom} />
            <GameDashboard />
            <TypingBlock />
            <StatisticsBlock />
            {timerExpired &&
                <Finish />
            }

        </Page>
    )
}

export default Game