import { useEffect } from "react"

const Login = () => {
    useEffect(() => {
        window.location.href = import.meta.env.VITE_API_URL + '/login';
    })

    return (<></>)
}

export default Login