import { useSelector } from 'react-redux';

import TypingBlock from '../../components/TypingBlock/Single';
import { H1, H3 } from '../../components/styled';
import { Page } from '../../components/styled';
import Finish from '../../modal/Finish/Single';
import StatisticsBlock from '../../components/StatisticsBlock'

import { timerExpiredSelector } from '../../store/timer/selectors';
import { userProfileSelector } from '../../store/user/selectors';
import { useEffect } from 'react';

const Home = () => {
    const timerExpired = useSelector(timerExpiredSelector);
    const authorized = useSelector(userProfileSelector);

    useEffect(() => {
        document.title = 'TypeSprint';
    }, [])

    return (
        <Page>
            <H1>{authorized ?
                'Test your typing skills' :
                'Log in.Type.Conquer!'
            }</H1>
            {!authorized &&
                <H3>Compete with your friends, track more stats, and top the leaderboard.</H3>
            }
            <TypingBlock />
            <StatisticsBlock />
            {timerExpired && <Finish />}

        </Page>
    );
};

export default Home;