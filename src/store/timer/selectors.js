import { sliceSelector } from "../../utils/redux/slices";
import { timerSlice } from "./slice";

export const timerSelector = sliceSelector(timerSlice);

export const timerExpiredSelector = state => timerSelector(state).timerExpired