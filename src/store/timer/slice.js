import { createSlice } from '@reduxjs/toolkit';

const initialState = {
    timeLeft: null,
    timerExpired: false,
    timerStarted: false,
    timerDuration: 60 * 1000,
};

export const timerSlice = createSlice({
    name: 'timer',
    initialState,
    reducers: {
        setTimeLeft: (state, action) => {
            state.timeLeft = action.payload;
        },
        clearTimeLeft: (state) => {
            state.timeLeft = null;
        },
        setTimerExpired: (state, action) => {
            state.timerExpired = action.payload;
        },
        setTimerStarted: (state, action) => {
            state.timerStarted = action.payload;
        },
        resetTimer: () => initialState,
    },
});

export const {
    setTimeLeft,
    clearTimeLeft,
    setTimerExpired,
    setTimerStarted,
    resetTimer,
} = timerSlice.actions;
export default timerSlice.reducer;