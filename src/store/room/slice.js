import { createSlice } from "@reduxjs/toolkit";
import { createRoomThunk } from "./thunk";
import { darkColors } from "../../assets/colors";
import { shuffle } from "../../utils/shuffleArray";

const colors = shuffle(Object.keys(darkColors));

const initialState = {
    id: null,
    created: false,
    members: [],
    state: null,
    hostMember: null,
    game: {
        words: [],
        initCharsCount: 0,
        typedWords: [],
        typedPart: '',
        currentWord: '',
        isCompleted: false,
        isCorrect: true,
        correctChars: 0,
        totalChars: 0,
        progress: 0,
    },
    timer: {
        timeLeft: null,
        timerExpired: false,
        timerStarted: false,
        timerDuration: 60 * 1000,
    }
};

const setGameProperty = (property, { game, ...rest }, { payload }) => {
    return {
        ...rest,
        game: {
            ...game,
            [property]: payload,
        }
    }
};

const arrayElementsLength = (array = []) => {
    return array.reduce((len, e) => len + e.length, 0)
}

export const roomSlice = createSlice({
    name: 'room',
    initialState,
    reducers: {
        setRoomId: (state, { payload: id }) => {
            state.id = id;
            state.created = true;
        },
        clearRoom: () => initialState,
        setRoom: (state, { payload: { members, words, ...rest } }) => {
            const membersIds = Object.values(state.members).map(m => m.userId);
            return {
                ...state,
                hostMember: members[rest.host],
                members: Object.values(members).map((e, i) => {
                    if (!(e.userId in membersIds)) {
                        e.color = colors[i % colors.length];
                    }
                    [e.fullName, e.name] = [e.name, e.name.split(' ')[0]]
                    return e;
                }),
                game: {
                    ...state.game,
                    words,
                    initCharsCount: arrayElementsLength(words),
                    currentWord: words ? words[0] : '',
                },
                ...rest
            };
        },
        clearGame: (state) => ({ ...state, game: initialState.game }),
        resetGameTimer: (state) => ({ ...state, timer: initialState.timer }),
        setGameWords: (state, action) => setGameProperty('words', state, action),
        setGameTypedWords: (state, action) => setGameProperty('typedWords', state, action),
        setGameTypedPart: (state, action) => setGameProperty('typedPart', state, action),
        setGameCurrentWord: (state, action) => setGameProperty('currentWord', state, action),
        setGameIsCompleted: (state, action) => setGameProperty('isCompleted', state, action),
        setGameIsCorrect: (state, action) => setGameProperty('isCorrect', state, action),
        setGameCorrectChars: (state, action) => setGameProperty('correctChars', state, action),
        setGameTotalChars: (state, action) => setGameProperty('totalChars', state, action),

        setGameTimeLeft: (state, action) => {
            state.timer.timeLeft = action.payload;
        },
        clearGameTimeLeft: (state) => {
            state.timer.timeLeft = null;
        },
        setGameTimerExpired: (state, action) => {
            state.timer.timerExpired = action.payload;
        },
        setGameTimerStarted: (state, action) => {
            state.timer.timerStarted = action.payload;
        },
        setGameProgress: ({ game, ...rest }) => {
            const { typedPart, typedWords, initCharsCount } = game;
            const progress = (typedPart.length + arrayElementsLength(typedWords)) * 100 / initCharsCount;
            return {
                ...rest,
                game: {
                    ...game,
                    progress,
                }
            }
        },
        setGameMembersProgress: ({ members, ...rest }, { payload }) => {
            const updated = members
                .map(m => {
                    const { progress, speedWPM, speedCPM, accuracy } = payload[m.userId] || m;
                    return {
                        ...m,
                        progress,
                        speedWPM,
                        speedCPM,
                        accuracy
                    }
                })
                .sort((a, b) => b.progress - a.progress);

            return {
                ...rest,
                members: updated
            }
        },

    },
    extraReducers: builder =>
        builder
            .addCase(createRoomThunk.fulfilled, (state, { payload: { roomId } }) => {
                state.created = true;
                state.id = roomId;
            })
            .addCase(createRoomThunk.rejected, (state) => {
                state.created = false;
                state.id = null;
            })
})

export const {
    setRoomId,
    clearRoom,
    addMember,
    setRoom,
    clearGame,
    setGameWords,
    setGameTypedWords,
    setGameTypedPart,
    setGameCurrentWord,
    setGameIsCompleted,
    setGameIsCorrect,
    setGameCorrectChars,
    setGameTotalTypedChars,
    setGameTotalChars,
    clearGameWordsData,
    setGameTimeLeft,
    clearGameTimeLeft,
    setGameTimerExpired,
    setGameTimerStarted,
    resetGameTimer,
    setGameProgress,
    setGameMembersProgress
} = roomSlice.actions;

export default roomSlice.reducer;