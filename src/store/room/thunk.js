import axios from "../../services/axios";
import { createAsyncThunk } from "@reduxjs/toolkit";

export const createRoomThunk = createAsyncThunk(
    'room/create',
    async () => {
        const { data: roomId } = await axios.post('/room/create');
        return roomId;
    }
);