import { sliceSelector } from "../../utils/redux/slices";
import { roomSlice } from "./slice";

export const roomSelector = sliceSelector(roomSlice);

export const roomIdSelector = (state) => roomSelector(state).id;

export const roomCreatedSelector = (state) => roomSelector(state).created;

export const roomMembersSelector = (state) => roomSelector(state).members;

export const roomHostSelector = (state) => roomSelector(state).host;

export const roomHostMemberSelector = (state) => roomSelector(state).hostMember;

export const roomStateSelector = (state) => roomSelector(state).state;

export const roomGameSelector = (state) => roomSelector(state).game;

export const roomTimerSelector = (state) => roomSelector(state).timer;