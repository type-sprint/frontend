import { sliceSelector } from "../../utils/redux/slices";
 import { userSlice } from "./slice";

export const userSelector = sliceSelector(userSlice);

export const userProfileSelector = state => userSelector(state).userProfile;

export const userIdSelector = state => userProfileSelector(state).userId;