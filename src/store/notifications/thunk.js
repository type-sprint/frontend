import { addNotification, removeNotification } from "./slice";

export const addNotificationWithTimeout = (notification) => (dispatch) => {
    const id = Date.now();

    dispatch(addNotification({ id, data: notification }));

    setTimeout(() => {
        dispatch(removeNotification(id));
    }, 5000);
};