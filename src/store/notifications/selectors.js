import { sliceSelector } from "../../utils/redux/slices";
import { notificationsSlice } from "./slice";

export const notificationsSelector = sliceSelector(notificationsSlice);

export const notificationListSelector = (state) => notificationsSelector(state).list;
