import { createSlice } from '@reduxjs/toolkit';

const initialState = { list: {} };

export const notificationsSlice = createSlice({
    name: 'notifications',
    initialState,
    reducers: {
        addNotification: (state, { payload: { id, data } }) => {
            state.list[id] = data;
        },
        removeNotification: (state, { payload: id }) => {
            delete state.list[id];
        },
        clearNotifications: () => initialState,
    },
});

export const {
    addNotification,
    removeNotification,
    clearNotifications,
} = notificationsSlice.actions;

export default notificationsSlice.reducer;