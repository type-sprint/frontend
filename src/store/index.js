import { configureStore } from '@reduxjs/toolkit';
import userReducer from './user/slice';
import timerReducer from './timer/slice';
import roomReducer from './room/slice';
import statisticsReducer from './statistics/slice';
import notificationsReducer from './notifications/slice';
import storage from 'redux-persist/lib/storage';
import { combineReducers } from 'redux';
import {
    persistReducer,
    FLUSH,
    REHYDRATE,
    PAUSE,
    PERSIST,
    PURGE,
    REGISTER,
} from 'redux-persist';

const reducer = combineReducers({
    user: persistReducer({ key: 'user', storage }, userReducer),
    timer: timerReducer,
    statistics: statisticsReducer,
    room: persistReducer({ key: 'room', storage }, roomReducer),
    notifications: notificationsReducer,
});

export default configureStore({
    reducer,
    middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware({
            serializableCheck: {
                ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
            },
        }),
});
