import { sliceSelector } from "../../utils/redux/slices";
import { statisticsSlice } from "./slice";

export const statisticsSelector = sliceSelector(statisticsSlice);
