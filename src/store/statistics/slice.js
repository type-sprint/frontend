import { createSlice } from '@reduxjs/toolkit';

const initialState = {
    speedWPM: 0,
    speedCPM: 0,
    accuracy: 0,
    level: 3
};

const levels = new Map([
    [{
        wpm: 45,
        cpm: 225,
        acc: 90
    }, 2],
    [{
        wpm: 55,
        cpm: 275,
        acc: 95
    }, 1],
    [{
        wpm: 65,
        cpm: 325,
        acc: 100
    }, 0]
])

export const statisticsSlice = createSlice({
    name: 'statistics',
    initialState,
    reducers: {
        setSpeedWPM: (state, { payload: { words, timeLeft, timerDuration } }) => {
            state.speedWPM = Math.ceil(words / (timerDuration - timeLeft) * 1000 * 60);
        },
        setSpeedCPM: (state, { payload: { chars, timeLeft, timerDuration } }) => {
            state.speedCPM = Math.ceil(chars / (timerDuration - timeLeft) * 1000 * 60);
        },
        resetStatistics: () => initialState,
        setAccuracy: (state, { payload: { correctChars, totalChars } }) => {
            const acc = Math.ceil(correctChars / totalChars * 100);
            state.accuracy = isNaN(acc) ? 0 : acc;
        },
        setLevel: ({ level: oldLevel, speedCPM, speedWPM, accuracy }) => {
            let level = oldLevel;
            for (let k of levels.keys()) {
                const { wpm, cpm, acc } = k;
                if (speedWPM > wpm && speedCPM > cpm && accuracy > acc) {
                    level = levels.get(k);
                }
            }
            return {
                level,
                speedCPM,
                speedWPM,
                accuracy
            }
        },
    }
})

export const {
    setSpeedWPM,
    setSpeedCPM,
    resetStatistics,
    setAccuracy,
    setLevel
} = statisticsSlice.actions

export default statisticsSlice.reducer